Generates an array of random chars to be used as the bitarray.
Then bitshifts a give amount by finding the modulo of the bitshift amount and 
rearranging the chars accordingly, then uses bitshift operator to deal with
the remaining shift needed. the bitshift operator is applied to every char
and was either shifted left or right

Then compares this method to the one given in MIT's 6.172: perfomance
engineering of software systems.