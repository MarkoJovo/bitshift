#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

//getBit(index)
//timer 
//
//
//
//Randomly populate char array
// copy twice
// bitshift with mine
// bitshift with MIT one
// compare by reading original, starting at bitshift index and wrap to start once end is reached


struct charArray{
	char* array;
	size_t len;
       	size_t bit_sz;	// length in bits
};

typedef struct charArray charArray;

	void charArray_Populate(charArray* c, size_t length){
		c->len = length;
		c->bit_sz = c->len*8;
		srand(time(0));
		c->array = malloc(c->len);
	       	unsigned char f;	// len bytes because 1 char = 1 byte
		for (int i = 0; i<c->len; i++){
			f = rand()%256;
			c->array[i] = f;
		}
	}


	void charArray_Copy(charArray* original, charArray* copy){ // Copy constructor
		copy->len = original->len;
		copy->bit_sz = original->len*8;
		copy->array = malloc(copy->len);
		for (int i=0; i<copy->len; i++){
			copy->array[i] = original->array[i];
		}
	}


	void charArray_free(charArray* c){
		free(c->array);
	}


void printByte(char c){
	printf("\n");
	for (size_t i = 8; i>0; i--){
		bool bitArrayGet = c & (1<<(i-1)) ? true : false;
		printf("%d", bitArrayGet);
	}
	printf("\n");
}


//shifts an array
void bit_shift_left_small(unsigned char* a, size_t byte_len, size_t amt){
        assert(amt<=8);
        size_t rs = 8-amt;
        size_t ls = amt;
        char frontMask = a[0]>>rs;
 	size_t i=byte_len-1;
	for (;i>0;i--){
		char leftMask = a[i]<<ls;
		char rightMask = a[i-1]>>rs;
                a[i] = leftMask|rightMask;
        }
	char backMask = a[i]<<ls;
	a[i] = frontMask|backMask;
}

char* bit_shift_left(char* a, size_t byte_len, size_t amt){
	if (amt/8 != 0){
		char* temp = malloc(byte_len);

		size_t i=amt/8;
		for (; i<byte_len; i++){
			//shift the character locations left by amt/8 
		//	printByte(a[i-amt/8]);
			temp[i] = a[i-amt/8];	
		}
		
		for(size_t j=0; j<amt/8; j++){
		//	printByte(a[i-amt/8]);
			temp[j] = a[i-amt/8];
			i++;
		}

		/*-----------------
		size_t i=0;
		for (; i<byte_len-amt/8; i++){
			//shift the character locations left by amt/8 
			printByte(a[i+amt/8]);
			temp[i] = a[i+amt/8];	
		}

		for(size_t j=0; j<amt/8; j++){
			printByte(a[j]);
			temp[i] = a[j];
			i++;
		}-----------------*/

		/*-------------------------
		size_t j = byte_len-1;
		size_t i = amt/8;
		for(; i<byte_len; i++){
			temp[j] = a[i];
			printf("%ld, %ld\n",i,j);
			j--;
		}
		
		for(i = 0; i<amt/8; i++){
			temp[j] = a[i];
			printf("%ld, %ld\n",i,j);
			j--;
		}
	-------------------------------------- */
	free(a);
	bit_shift_left_small(temp, byte_len, amt%8);
	return temp;
	}
}

//----- lifted from MIT 6.172 course, performance engineering of software systems 	-----
static char bitmask(size_t bit_index) {
  return 1 << (bit_index % 8);
}

bool bitarray_get(charArray *ca, size_t bit_index) {
  assert(bit_index < ca->bit_sz);
  return (ca->array[bit_index / 8] & bitmask(bit_index)) ? true : false;
}

void bitarray_set(charArray *ca, size_t bit_index, bool val) {
  assert(bit_index < ca->bit_sz);
  ca->array[bit_index / 8] 
	  = (ca->array[bit_index / 8] & ~bitmask(bit_index)) | (val ? bitmask(bit_index) : 0); 
}

//-----	 				---						-----	

void printBits(charArray* c){
	printf("\n");
	for (size_t i = c->bit_sz; i>0; i--){
		if (i%8==0) printf(" ");
		printf("%d", bitarray_get(c,i-1));
	}
	printf("\n");
}

bool shiftTest(charArray* origin, charArray* test, size_t amtbyte){
	int j = test->bit_sz-1;
	size_t amt = amtbyte*8;
	assert(origin->bit_sz == test->bit_sz);

	for (int i=amt; i<test->bit_sz; i++){
		if (bitarray_get(test, j) != bitarray_get(origin, i));
		       	return false;
		j--;
	}

	for (int i=0; i<amt; i++){
		if (bitarray_get(test,j) != bitarray_get(origin,i));
			return false;
		j--;
	}

	return true;
	
}
int main(){
	charArray original;
	charArray* op = &original;
	charArray_Populate(op,10000000);
	int shiftAmount = 50000*8;
	charArray fastShift;
	charArray* fSp = &fastShift;
	charArray_Copy(op,fSp);

	fSp->array = bit_shift_left(fastShift.array, fastShift.len, shiftAmount/8);
	bool res = shiftTest(op, fSp,shiftAmount);
	printf("\nShifted left by: %d\n", shiftAmount);
//	printBits(op);
//	printBits(fSp);




	printf("Success? 1 = yes, 0 = no:  %d\n", res); 

	return 0;
}
